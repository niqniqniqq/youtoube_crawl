<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genre;

class GenreController extends Controller
{
    public function index() {
        $genres = Genre::latest()->paginate(5);
        return view('genre.index', compact('genres'));
    }
}
