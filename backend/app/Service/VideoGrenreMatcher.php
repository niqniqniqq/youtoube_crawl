<?php
namespace App\Service;

use Illuminate\Support\Facades\DB;
use function PHPUnit\Framework\isNull;
use App\Models\YoutubeVideoGrenre;
use App\Models\Genre;

class VideoGrenreMatcher
{

    private $mecab;

    function __construct()
    {
        $this->mecab = new \Mecab\Tagger();
    }

    public function test($text)
    {
        $this->getkeyword($text);
    }

    public function matchVideoGenre()
    {
        $videos = $this->getUnmatchedVideos();
        foreach ($target_videos as $video) {
            $keyword = $this->getkeyword($video->description);
            $genres = Genre::getKeyWordMatchedRecord($keyword);
        }
    }

    private function getUnmatchedVideos()
    {
        DB::table('youtoube_videos')->whereNotExists(function ($query) {
            $query->select(DB::raw(1))
                ->from('youtube_video_genres')
                ->whereRaw('youtoube_videos.id = youtube_video_genres.youtube_video_id');
        })
            ->get();
    }

    private function getkeyword($text)
    {
        // 形態素解析し、名刺を半角スペイースで繋いだ文字列を返す
        $returnArray = [];
        $beforeSurface = '';
        # 半角スペースだとmecabに握り潰されるので一度全角にする。
        $text = mb_convert_kana($text, 'S');
        $nodes = $this->mecab->parseToNode($text);
        foreach ($nodes as $node) {
            $surface = $node->getSurface();
            $feature = explode(',', $node->getFeature())[0];
            if ($feature != '名詞') {
                $beforeSurface = '';
                continue;
            }

            # ハッシュタグ系処理
            if ($this->mbTrim($surface) == '#') {
                $beforeSurface = '#';
                if (strncmp($beforeSurface, '#', 1) == 0) {
                # 前回ループ時に保存ずみ
                continue;
                }
            } else {
                $surface = $beforeSurface . $surface;
                $beforeSurface = $surface;
            }

            var_dump($surface);
            echo '######';
        }
        return '';
    }

    private function mbTrim($pString)
    {
        return preg_replace('/\A[\p{Cc}\p{Cf}\p{Z}]++|[\p{Cc}\p{Cf}\p{Z}]++\z/u', '', $pString);
    }
}