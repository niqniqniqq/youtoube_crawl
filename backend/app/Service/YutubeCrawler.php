<?php
namespace App\Service;

use function PHPUnit\Framework\isNull;
use Google_Client;
use Google_Exception;
use Google_Service_Exception;

class YutubeCrawler
{

    private $youtubeClient = null;

    const DEFAULT_PARAMS = [
        'type' => 'video',
        'maxResults' => '20',
        'order' => 'date'
    ];

    function __construct($projectName = "", $apiKey = "")
    {
        // $this->params['publishedAfter'] = date("Y-m-d\TH:i:sP", strtotime("-1 week"));
        $googleClient = new Google_Client();
        $googleClient->setApplicationName($projectName);
        $googleClient->setDeveloperKey($apiKey);
        $this->youtubeClient = new \Google_Service_YouTube($googleClient);
    }

    public function startCrawlByKeyword($keyword)
    {
        $returnArry = [];
        if (is_null($this->youtubeClient)) {
            return $returnArry;
        }

        if (is_null($keyword)) {
            return $returnArry;
        }

        $params = [
            'q' => $keyword
        ];
        $params = array_merge(self::DEFAULT_PARAMS, $params);
        $response = $this->getApiResponse($params);
        $returnArry = $this->parseResponse($response);

        return $returnArry;
    }

    public function startCrawlByCannel($channelId)
    {
        $returnArry = [];
        if (is_null($this->youtubeClient)) {
            return $returnArry;
        }

        if (is_null($channelId)) {
            return $returnArry;
        }

        $params = [
            'channelId' => $channelId
        ];
        $params = array_merge(self::DEFAULT_PARAMS, $params);
        $response = $this->getApiResponse($params);
        $returnArry = $this->parseResponse($response);

        return $returnArry;
    }

    private function getApiResponse($params)
    {
        $searchResponse = [];
        try {
            $searchResponse = $this->youtubeClient->search->listSearch('snippet', $params);
        } catch (Google_Service_Exception $e) {
            echo htmlspecialchars($e->getMessage());
            return;
        } catch (Google_Exception $e) {
            echo htmlspecialchars($e->getMessage());
            return;
        }

        return $searchResponse;
    }

    private function parseResponse($responses = [])
    {
        $returnArry = [];
        if (empty($responses)) {
            return $returnArry;
        }

        $items = array_get($responses, 'items', []);
        if (empty($items)) {
            return $returnArry;
        }

        foreach ($items as $item) {
            $temp = [];
            $id = array_get($item, 'id', []);
            $snippet = array_get($item, 'snippet', []);
            $thumbnails = array_get($snippet, 'thumbnails', []);
            $temp['video_id'] = array_get($id, 'videoId', '');
            $temp['channel_id'] = array_get($snippet, 'channelId', '');
            $temp['channel_title'] = array_get($snippet, 'channelTitle', '');
            $temp['title'] = array_get($snippet, 'title', '');
            $temp['description'] = array_get($snippet, 'description', '');
            $temp['thumbnail_url'] = array_get($thumbnails, 'default.url', '');
            $temp['thumbnail_url_m'] = array_get($thumbnails, 'medium.url', '');
            $temp['thumbnail_url_h'] = array_get($thumbnails, 'high.url', '');
            $temp['published_at'] = date('Y-m-d H:i:s', strtotime(array_get($snippet, 'publishTime', '')));
            $returnArry[] = $temp;
        }

        return $returnArry;
    }
}