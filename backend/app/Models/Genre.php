<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{

    public static function getKeyWordMatchedRecord($keyword)
    {
        if (empty($keyword)) {
            return [];
        }

        return self::whereRaw("match(`keyword`) against (? IN NATURAL LANGUAGE MODE)", [
            $keyword
        ])->get();
    }
}
