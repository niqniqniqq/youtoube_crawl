<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YoutubeVideoGrenre extends Model
{

    protected $fillable = [
        'youtube_video_id',
        'genre_id'
    ];

    public static function storeDatas($datas)
    {
        foreach ($datas as $data){
            $yutubeVideoGenre = self::firstOrNew(['youtube_video_id' => $data['youtube_video_id'], 'genre_id' => $data['genre_id']]);
            try{
                $yutubeVideoGenre->save();
            }
            catch(\Exception $e){
                echo $e->getMessage();
            }
        }
    }

    public static function getActiveRecord()
    {
        return self::where('is_deleted', 0)->get();
    }
}
