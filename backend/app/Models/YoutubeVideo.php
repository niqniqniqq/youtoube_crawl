<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YoutubeVideo extends Model
{

    protected $fillable= ['video_id'];

    public static function storeDatas($datas)
    {
        foreach ($datas as $data){
            if (empty($data['video_id'])) {
                continue;
            }
            $video = self::firstOrNew(['video_id' => $data['video_id']]);
            $video->channel_id = $data['channel_id'];
            $video->channel_title = $data['channel_title'];
            $video->title = $data['title'];
            $video->description = $data['description'];
            $video->thumbnail_url = $data['thumbnail_url'];
            $video->thumbnail_url_m = $data['thumbnail_url_m'];
            $video->thumbnail_url_h = $data['thumbnail_url_h'];
            $video->published_at = $data['published_at'];
            try{
                $video->save();
            }
            catch(\Exception $e){
                echo $e->getMessage();
            }
        }
    }
}
