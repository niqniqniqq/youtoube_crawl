<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Service\YutubeCrawler;
use App\Models\YoutubeVideo;

class CrawlYoutubeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:crawl-youtube';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'crawl youtube and store DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $crawlerService = new YutubeCrawler('crawler', 'AIzaSyAm2v-i-OSOrPSSYSpyrt0HJx0zhgsuxlI');
        $datas = $crawlerService->startCrawlByCannel('UCOvfPKigJRpQMdfH-lbO3Cg');
        YoutubeVideo::storeDatas($datas);
        $datas = $crawlerService->startCrawlByKeyword('バイク女子 -ロードバイク');
        var_dump($datas);
        return 0;
    }
}
