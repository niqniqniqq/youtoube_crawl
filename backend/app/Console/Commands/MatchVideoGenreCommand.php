<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Service\VideoGrenreMatcher;

class MatchVideoGenreCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:match-genre';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $service = new VideoGrenreMatcher();
        $service->test("バイク女子、初ツーリングでキャンプする。まさかの立ちゴケ。外装ならし。NINJA400 　バックステップ取り付け #バイク女子 #motovlog #motoblog");
        return 0;
    }


}
