<?php
namespace Database\Seeders;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{

    public function run()
    {
       $regular_user = new User();
        $regular_user->name = 'sample';
        $regular_user->email = 'sample@sample.com';
        $regular_user->password = Hash::make('regular');
        $regular_user->save();

        $admin_user = new User();
        $admin_user->name = 'admin';
        $admin_user->email = 'admin@admin.com';
        $admin_user->password = Hash::make('admin');
        $admin_user->save();
    }
}
