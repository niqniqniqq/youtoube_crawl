<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFeedChannelGenresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_channel_genres', function (Blueprint $table) {
            $table->id();
            $table->integer('genre_id');
            $table->integer('feed_channel_id');
            $table->tinyInteger('is_deleted');
            $table->timestamps();
            $table->index('is_deleted');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed_channel_genres');
    }
}
