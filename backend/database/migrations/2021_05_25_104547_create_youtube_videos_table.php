<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYoutubeVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('youtube_videos', function (Blueprint $table) {
            $table->id();
            $table->string('video_id', 100);
            $table->string('channel_id', 100);
            $table->string('channel_title', 100);
            $table->string('title', 100);
            $table->string('description', 255);
            $table->string('thumbnail_url', 512);
            $table->string('thumbnail_url_m', 512);
            $table->string('thumbnail_url_h', 512);
            $table->dateTime('published_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('youtoube_videos');
    }
}
